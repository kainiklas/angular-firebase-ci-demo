# AngularFirebaseCiDemo

Demo of how to automatically build, test and deploy an *Angular 6* application to *Firebase* with *Bitbucket Pipelines*. 

More information can be found here: 

* [CI/CD with Angular 6 & Firebase & Bitbucket Pipelines](https://medium.com/quick-code/ci-cd-with-angular-6-firebase-bitbucket-pipelines-a72a5445ef6f)

## Build for Production

Run `npm run build-prod` to build the project for productive usage. The build artifacts will be stored in the `dist/` directory.

## Test

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Deployment

Run `npm run deployment` to deploy to [Firebase](https://firebase.google.com/).